package by.stormnet.generics;

public class Student implements IPrintable {
    private String name;
    private String firstName;
    private int age;
    private Gender gender;

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", firstName='" + firstName + '\'' +
                ", age=" + age +
                ", gender=" + gender +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Student student = (Student) o;

        if (age != student.age) return false;
        if (!name.equals(student.name)) return false;
        if (!firstName.equals(student.firstName)) return false;
        return gender == student.gender;
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + firstName.hashCode();
        result = 31 * result + age;
        result = 31 * result + gender.hashCode();
        return result;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Student(String name, String firstName, int age, Gender gender) {

        this.name = name;
        this.firstName = firstName;
        this.age = age;
        this.gender = gender;
    }

    @Override
    public void print() {
        System.out.println(toString());
    }

    public enum Gender{
        MALE,
        FEMALE;
 }
}
