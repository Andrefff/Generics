package by.stormnet.generics;

public class MyGenericType<T extends IPrintable> {
    private T[] objects;

    public MyGenericType() {
    }

    public MyGenericType(T[] objects) {

        this.objects = objects;
    }

    public void addObject(T obj,int index){
        if(this.objects!=null){
            this.objects[index] = obj;
        }
    }
    public T getObject(int index){
        T obj = objects[index];

        return obj;
    }
    public void printAllInfo(){
        if(objects == null || objects.length==0){
            System.out.println("nothing to print");
        return;}

        for(T obj:objects){
            if(obj==null)
                continue;
            obj.print();
            System.out.println("\n=======================\n");
        }
    }
}
