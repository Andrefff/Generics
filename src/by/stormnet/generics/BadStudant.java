package by.stormnet.generics;

/**
 * Created by java4 on 22.8.17.
 */
public class BadStudant extends Student {
    public BadStudant(String name, String firstName, int age, Gender gender) {
        super(name, firstName, age, gender);
    }

    @Override
    public void print() {
        System.out.println("I am bad Studant");
    }
}
