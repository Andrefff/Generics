package by.stormnet.generics;

public class Application {
    public static void main(String[] args) {
        Application app =new Application();
        app.runTheApp();
    }

    private void runTheApp() {

        MyGenericType<Student> myGenericTypeStud=
                new MyGenericType<>(new Student[10]);
        myGenericTypeStud.addObject(new Student("Vasya","Pupkin",15, Student.Gender.FEMALE),0);
        myGenericTypeStud.addObject(new Student("Masha","Pupkin",15, Student.Gender.FEMALE),1);
        myGenericTypeStud.addObject(new Student("Lena","Pupkin",15, Student.Gender.FEMALE),2);
        myGenericTypeStud.addObject(new BadStudant("Loh","loh",15,Student.Gender.MALE),3);
        myGenericTypeStud.printAllInfo();
    }
}
